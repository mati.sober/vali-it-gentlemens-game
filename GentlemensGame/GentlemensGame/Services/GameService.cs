﻿using GentlemensGame.Data;
using GentlemensGame.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Services
{
    public class GameService: IGameService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _user;

        public GameService(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IHttpContextAccessor user)
        {
            _context = context;
            _userManager = userManager;
            _user = user;
        }


        public async Task AddPlayer(int id)
        {
            var newPlayer = new GamePlayer() { GameId = id, UserId = _userManager.GetUserAsync(_user.HttpContext.User).Result.Id };
            _context.GamePlayer.Add(newPlayer);
            await _context.SaveChangesAsync();

          
        }

        public int TuckWinnerId()
        {
            var tucks = _context.Tuck.ToList();
            List<int> cards = tucks.Select(x => x.CardId).ToList();
            var cardOne = cards[0];
            var cardTwo = cards[1];
            var cardThree = cards[2];

            if (cardOne > cardTwo && cardOne > cardThree)
            {
                return cardOne;
            }
            else if (cardTwo > cardOne && cardTwo > cardThree)
            {
                return cardTwo;
            }
            else
                return cardThree;

        }

        public List<Hand> MakePlayerHand(int id)
        {
            List<CardDeck> cards = _context.CardDeck.Where(x => x.GameId == id).ToList();
            List<Hand> playerHand = new List<Hand>();
            int counter = 0;
            var userId = _context.GamePlayer.FirstOrDefault(x => x.UserId == _userManager.GetUserAsync(_user.HttpContext.User).Result.Id).Id;
            var handNow = _context.Hand.Where(x => x.GamePlayerId == userId).ToList();
            _context.RemoveRange(handNow);

            foreach (var item in cards.ToList())
            {

                if (counter < 5)
                {
                    counter++;
                    playerHand.Add(new Hand() { CardId = item.CardId, GamePlayerId = userId });
                    cards.Remove(cards.FirstOrDefault(x => x.CardId == item.CardId));
                    _context.CardDeck.Remove(item.Game.CardDecks.FirstOrDefault(x => x.CardId == item.CardId));
                }

            }
            _context.Hand.AddRange(playerHand);
            _context.SaveChanges();
            return playerHand;
        }

        private bool GameExists(int id)
        {
            return _context.Game.Any(e => e.Id == id);
        }

        public async Task DeleteCardFromHandAndAddToTuck(int cardId)
        {
            var userId = _context.GamePlayer.FirstOrDefault(x => x.UserId == _userManager.GetUserAsync(_user.HttpContext.User).Result.Id).Id;
            var cardPlayed = _context.Hand.FirstOrDefault(x => x.GamePlayerId == userId && x.CardId == cardId);
            Tuck tuck = new Tuck() { CardId = cardPlayed.CardId, GameplayerId = userId };

            _context.Hand.Remove(cardPlayed);
            _context.Tuck.Add(tuck);
            await _context.SaveChangesAsync();
        }

        
    }
}
