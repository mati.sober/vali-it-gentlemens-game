﻿using GentlemensGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Services
{
    public interface IGameService
    {
        List<Hand> MakePlayerHand(int id);
        Task AddPlayer(int id);
        Task DeleteCardFromHandAndAddToTuck(int cardId);
    }
}
