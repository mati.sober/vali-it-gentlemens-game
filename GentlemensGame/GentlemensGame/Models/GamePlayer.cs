﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Models
{
    public class GamePlayer
    {
        public int Id { get; set; }
        public Game Game { get; set; }
        public int GameId { get; set; }
        
        public string UserId { get; set; }
        public int PlayerScore { get; set; }

        public List<Hand> Hands { get; set; }
       
    }
 
}
