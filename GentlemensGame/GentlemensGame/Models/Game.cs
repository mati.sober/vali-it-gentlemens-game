﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string WinnerId { get; set; }
        public bool FinishedGame { get; set; }
       
        [NotMapped]
        public string FullName {
            get
            {
                return Id + ". " + Name;
            }
        }

        public List<CardDeck> CardDecks { get; set; }
    }
}
