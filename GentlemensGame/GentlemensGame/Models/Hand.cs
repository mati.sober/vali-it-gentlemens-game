﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Models
{
    public class Hand
    {
        public int Id { get; set; }

        public int GamePlayerId { get; set; }
        public GamePlayer GamePlayer { get; set; }

        public int CardId { get; set; }
        public Card Card { get; set; }

       
    }
}
