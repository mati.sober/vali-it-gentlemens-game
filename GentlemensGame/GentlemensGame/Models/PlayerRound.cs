﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace GentlemensGame.Models
{

    public class PlayerRound
    {
        public int Id { get; set; }
        public Round Round { get; set; }
        public int RoundId { get; set; }

        public GamePlayer GamePlayer{ get; set; }
        public int GamePlayerId { get; set; }

        public int WantedTucks { get; set; }
        public int GotTucks { get; set; }
    }
}
