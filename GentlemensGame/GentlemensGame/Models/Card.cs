﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace GentlemensGame.Models
{
    public class Card
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string CardSuit { get; set; }
        public int CardValue { get; set; }

        public List<CardDeck> CardDecks { get; set; }
    }

   
}
