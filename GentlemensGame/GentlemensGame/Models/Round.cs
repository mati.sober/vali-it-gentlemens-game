﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Models
{
    public class Round
    {
        public int Id { get; set; }
        public int RoundNumber { get; set; }
        public string DealerId { get; set; }

        public Game Game { get; set; }
        public int? GameId{ get; set; }

        public Card Trump { get; set; }
        public int TrumpId { get; set; }        
        

    }
}
