﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GentlemensGame.Models
{
    public class CardDeck
    {
        public int Id { get; set; }

        public Game Game { get; set; }
        public int GameId { get; set; }

        public int CardId { get; set; }
        public Card Card { get; set; }
    }
}
