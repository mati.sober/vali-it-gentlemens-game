﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;


connection.on("ReceiveMessage", function (user, message) {
    $('#messagesList').prepend('<img src="' + message + '" height="150" />');
    $('#userList').html("Last card was played by: " + "<strong>" + user + "</strong>");
    $('#' + user).children().unbind('click', imgClick);
    tuck();
    
});

connection.start().then(function () {
    var groupName = document.getElementById("gameName").innerHTML;
    connection.invoke("AddToGroup", groupName);
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
    
    
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    var groupName = document.getElementById("gameName").innerHTML;
    connection.invoke("SendMessage", groupName, user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
    
});

