﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace GentlemensGameTesting.Hubs
{
    public class ChatHub : Hub
    {
        public  Task SendMessage(string groupName, string user,  string message)
        {

            return  Clients.Group(groupName).SendAsync("ReceiveMessage",user, message);
           
        }

        public async Task AddToGroup(string groupName)
        {
          await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
   
        }

        public async Task RemoveFromGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);

        }
        
    }
}
