﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using GentlemensGame.Models;

namespace GentlemensGame.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<CardDeck>().HasKey(x => new { x.GameId, x.CardId });
        //}


        public DbSet<Card> Card { get; set; }
        public DbSet<CardDeck> CardDeck { get; set; }
        public DbSet<Tuck> Tuck { get; set; }
        public DbSet<Round> Round { get; set; }
        public DbSet<GamePlayer> GamePlayer { get; set; }
        public DbSet<Hand> Hand { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<PlayerRound> PlayerRound { get; set; }
    }
}
