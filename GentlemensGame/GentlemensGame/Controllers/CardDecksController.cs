﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GentlemensGame.Data;
using GentlemensGame.Models;
using Microsoft.Extensions.Configuration;

namespace GentlemensGame.Controllers
{
    public class CardDecksController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public CardDecksController(ApplicationDbContext context, IConfiguration config)
        {
            _context = context;
            _configuration = config;
        }

        // GET: CardDecks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.CardDeck.Include(c => c.Card);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: CardDecks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cardDeck = await _context.CardDeck
                .Include(c => c.Card)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cardDeck == null)
            {
                return NotFound();
            }

            return View(cardDeck);
        }

        // GET: CardDecks/Create
        public IActionResult Create()
        {
            ViewData["CardId"] = new SelectList(_context.Card, "Id", "Id");
            return View();
        }

        // POST: CardDecks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,GameId,CardId")] CardDeck cardDeck)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cardDeck);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CardId"] = new SelectList(_context.Card, "Id", "Id", cardDeck.CardId);
            return View(cardDeck);
        }

        // GET: CardDecks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cardDeck = await _context.CardDeck.FindAsync(id);
            if (cardDeck == null)
            {
                return NotFound();
            }
            ViewData["CardId"] = new SelectList(_context.Card, "Id", "Id", cardDeck.CardId);
            return View(cardDeck);
        }

        // POST: CardDecks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,GameId,CardId")] CardDeck cardDeck)
        {
            if (id != cardDeck.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cardDeck);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CardDeckExists(cardDeck.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CardId"] = new SelectList(_context.Card, "Id", "Id", cardDeck.CardId);
            return View(cardDeck);
        }

        // GET: CardDecks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cardDeck = await _context.CardDeck
                .Include(c => c.Card)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cardDeck == null)
            {
                return NotFound();
            }

            return View(cardDeck);
        }

        // POST: CardDecks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cardDeck = await _context.CardDeck.FindAsync(id);
            _context.CardDeck.Remove(cardDeck);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CardDeckExists(int id)
        {
            return _context.CardDeck.Any(e => e.Id == id);
        }


       
    }
}
