﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GentlemensGame.Data;
using GentlemensGame.Models;
using Microsoft.AspNetCore.Authorization;

namespace GentlemensGame.Controllers
{
    [Authorize]
    public class CardsController : Controller
    {
        private readonly ApplicationDbContext _context;

        
        public CardsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Cards
        public IActionResult Index()
        {
   
            if(!_context.Card.Any())
            {
                InitialCardsToDb();
            }

            return RedirectToAction("Index", "Games");
        }

        private void InitialCardsToDb()
        {
            List<Card> cards = new List<Card>();

            string[] suits = new string[] { "Clubs", "Hearts", "Spades", "Diamonds" };
            int[] values = new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14 };
            int id = 1;
            foreach (var suit in suits)
            {
                foreach (var value in values)
                {
                    cards.Add(new Card() { CardSuit = suit, CardValue = value, Id = id });
                    id++;
                }
            }


            var cardInDb = _context.Card.ToList();
            _context.RemoveRange(cardInDb);

            foreach (var item in cards)
            {
                _context.Add(item);
                _context.SaveChanges();
            }
        }
    }
}
