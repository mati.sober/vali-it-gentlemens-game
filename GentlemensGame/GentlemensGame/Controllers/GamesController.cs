﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GentlemensGame.Data;
using GentlemensGame.Models;
using Microsoft.AspNetCore.Identity;
using GentlemensGame.Services;

namespace GentlemensGame.Controllers
{
    public class GamesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGameService _gameService;

        public GamesController(UserManager<ApplicationUser> userManager, ApplicationDbContext context, IGameService gameService)
        {
            _context = context;
            _userManager = userManager;
            _gameService = gameService;
        }

        // GET: Games/
        public async Task<IActionResult> Index()
        {
            ViewData["GameId"] = new SelectList(_context.Game, "Id", "FullName");
            return View(await _context.Game.ToListAsync());

        }

        public async Task<IActionResult> Game(int? id)
        {
            ViewBag.GameId = _context.Game.FirstOrDefault(x => x.Id == id);

            return await PlayerHand((int)id);

        }

       

        public async Task<IActionResult> DelCardAddTuck([Bind("Id,GamePlayerId, CardId")] int cardId)
        {
            await _gameService.DeleteCardFromHandAndAddToTuck(cardId);
            return View();
        }

       


        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,WinnerId")] Game game)
        {

            _context.Add(game);
            await _context.SaveChangesAsync();
            return Json(game);

        }




        [HttpPost]
        public async Task <IActionResult> AddGamePlayer([Bind("Id,UserId,GameId")] int id)
        {
            await _gameService.AddPlayer(id);
            return View();

        }

        private async Task<IActionResult> PlayerHand(int? id)
        {
            if (id != null)
            {
                if (_context.CardDeck.FirstOrDefault(x => x.GameId == id) != null)
                {
                    var playerHand = _gameService.MakePlayerHand((int)id);
                    return View(playerHand);
                }

                var cards = await _context.Card.OrderBy(x => Guid.NewGuid())
                    .Select(x => new CardDeck() { CardId = x.Id, GameId = (int)id }).ToListAsync();

                _context.AddRange(cards);
                await _context.SaveChangesAsync();

                var initialHand = _gameService.MakePlayerHand((int)id);
                return View(initialHand);
            }


            return View();
        }



    }   
}
