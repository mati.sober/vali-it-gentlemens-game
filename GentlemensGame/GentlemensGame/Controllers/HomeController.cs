﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GentlemensGame.Models;
using GentlemensGame.Areas.Identity.Pages.Account;
using Microsoft.AspNetCore.Identity;
using GentlemensGame.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GentlemensGame.Controllers
{
    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public HomeController(SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            _signInManager = signInManager;
            _context = context;
          

        }
        public IActionResult Index()
        {   
            
           if(User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Games");
            }
            return View();

        }

        public IActionResult Score()
        {
            ViewData["Message"] = "Scoreboard";

            return View();
        }

        public IActionResult Chat()
        {
            return View();
        }

        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            var result = await _signInManager.PasswordSignInAsync(loginModel.Input.Email, loginModel.Input.Password, true, true);
            if(!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return View("Index", loginModel);
            }

            return RedirectToAction("Index", "Games");

        }
    }
}
